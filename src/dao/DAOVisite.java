package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Visite;

public class DAOVisite {
	public ArrayList<Visite> select() throws SQLException, ClassNotFoundException{
		ArrayList<Visite>  listeV = new ArrayList<Visite> () ;
		String sql = "select * from visites" ;
		
		Class.forName("com.mysql.jdbc.Driver") ; 
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/projethopital", "root", "root" ); 
		
		Statement st=conn.createStatement(); 
		
		ResultSet rs = st.executeQuery(sql); 
		
		while (rs.next()) 
		{
			Visite v =  new Visite (rs.getInt("id"), rs.getDate("date"),  rs.getString("medecin"), rs.getInt("numeroSalle"), rs.getInt("idPatient"));
			listeV.add(v);
		}
		
		conn.close();
		
		return listeV ;
	}
	
	public Visite selectById(int id) throws SQLException, ClassNotFoundException{
		Visite v = null ;
		String sql = "select * from visites where id = '" + id + "'" ;
		
		Class.forName("com.mysql.jdbc.Driver") ; 
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/projethopital", "root", "root" ); 
		
		Statement st=conn.createStatement(); 
		
		ResultSet rs = st.executeQuery(sql); 
		
		if (rs.next()) 
		{
			v =  new Visite  (rs.getInt("id"), rs.getDate("date"),  rs.getString("medecin"), rs.getInt("numeroSalle"), rs.getInt("idPatient"));
		}
		
		conn.close();
		
		return v ;
	}
	
	public ArrayList<Visite> selectByIdPatient(int idPatient) throws SQLException, ClassNotFoundException{
		ArrayList<Visite>  listeV = new ArrayList<Visite> () ;
		Visite v = null ;
		String sql = "select * from visites where idPatient = " + idPatient ;
		
		Class.forName("com.mysql.jdbc.Driver") ; 
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/projethopital", "root", "root" ); 
		
		Statement st=conn.createStatement(); 
		
		ResultSet rs = st.executeQuery(sql); 
		
		if (rs.next()) 
		{
			v =  new Visite  (rs.getInt("id"), rs.getDate("date"),  rs.getString("medecin"), rs.getInt("numeroSalle"), rs.getInt("idPatient"));
			listeV.add(v);
		}
		
		conn.close();
		
		return listeV ;
	}
	
	public void insert(Visite v) throws ClassNotFoundException, SQLException{
		String sql = "insert into visites (date, medecin, numeroSalle, idPatient) values (?, ?, ?, ?)";
		
		Class.forName("com.mysql.jdbc.Driver") ; 
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/projetHopital", "root", "root" ); 
		
		
		PreparedStatement ps= conn.prepareStatement(sql); 
		ps.setString(1,  v.getStringDate()) ;
		ps.setString (2, v.getMedecin()) ;
		ps.setInt (3, v.getNumSalle()) ;
		ps.setInt(4, v.getIdPatient()) ;
				
		ps.executeUpdate(); 
		conn.close();
	}
	
	public void update(Visite v) throws ClassNotFoundException, SQLException{
		String sql = "update visites set  date = '"+ v.getDate() + "', medecin ='" + v.getMedecin() + "', numeroSalle =" + v.getNumSalle()  +  
				", idPatient =" + v.getIdPatient() + " where id = " + v.getId();
		
		Class.forName("com.mysql.jdbc.Driver") ; 
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/projethopital", "root", "root" ); 
		
		Statement st=conn.createStatement(); 
		
		st.executeUpdate(sql); 
		
		conn.close();
	}
	
	
	public void delete(int id) throws ClassNotFoundException, SQLException{
		String sql = "delete from visites  where id=" + id;
		
		Class.forName("com.mysql.jdbc.Driver") ; 
		Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/projethopital", "root", "root" ); 
		
		Statement st=conn.createStatement(); 
		
		st.executeUpdate(sql); 
		
		conn.close();
	}
}
