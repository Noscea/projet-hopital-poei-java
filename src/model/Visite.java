package model;

import java.util.Date;

public class Visite {
	private int id;
	private Date date;
	private String medecin;
	private int numSalle;
	final int tarif = 23;	
	private int idPatient;
	
	
	public Visite(int id, Date date, String medecin, int numSalle, int idPatient) {
		this.id = id;
		this.date = date;
		this.medecin = medecin;
		this.numSalle = numSalle;
		this.idPatient = idPatient;
	}
	
// Constructeur d'une instance de Visite sans ID (incr�ment automatique) et sans date (pour horodatage)
	public Visite(String medecin, int numSalle, int idPatient) {
		this.date = new Date();
		this.medecin = medecin;
		this.numSalle = numSalle;
		this.idPatient = idPatient;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}



	public String getStringDate() {
		return date.toString();
	}
	
	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getMedecin() {
		return medecin;
	}


	public void setMedecin(String medecin) {
		this.medecin = medecin;
	}


	public int getNumSalle() {
		return numSalle;
	}


	public void setNumSalle(int numSalle) {
		this.numSalle = numSalle;
	}


	public int getIdPatient() {
		return idPatient;
	}


	public void setIdPatient(int idPatient) {
		this.idPatient = idPatient;
	}


	public int getTarif() {
		return tarif;
	}


	@Override
	public String toString() {
		return "Visite [id=" + id + ", date=" + date + ", medecin=" + medecin + ", numSalle=" + numSalle + ", tarif="
				+ tarif + ", idPatient=" + idPatient + "]";
	}

	
	
	
	

	
	



	
	
	
	
}
