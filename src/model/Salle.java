package model;

public class Salle {
	
	private Medecin medecin;
	private Patient patient;
	
	private boolean disponibilité;
	private final int numeroSalle;
	
	public Salle(int numeroSalle) {
		this.disponibilité = false;
		this.numeroSalle = numeroSalle;
	}

	public Medecin getMedecin() {
		return medecin;
	}

	public void setMedecin(Medecin medecin) {
		this.medecin = medecin;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public boolean isDisponibilité() {
		return disponibilité;
	}

	public void setDisponibilité(boolean disponibilité) {
		this.disponibilité = disponibilité;
	}

	public int getNumeroSalle() {
		return numeroSalle;
	}
	
	public void changePatient(){
		this.patient = Hopital.appellerProchainPatient();
		
		System.out.println(this.patient + "est entré en salle" + this.numeroSalle);
		this.disponibilité = false;
	}
	
	public void DemandeNouveauPatient() {
		this.setDisponibilité(true);
		this.changePatient();
	}

}
