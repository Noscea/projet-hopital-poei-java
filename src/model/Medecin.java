package model;

import java.sql.SQLException;


import dao.DAOVisite;

public class Medecin extends Personnel {
	
	private int maxVisites = 10;
	private Visite[] listeVisites;
	private int compteurVisites;
	private Salle salle;

	public Medecin(int id, String nom, String prenom, int metier) {
		super(id, nom, prenom, metier);
		this.listeVisites =  new Visite[maxVisites];
		this.compteurVisites = 0;
		this.salle = Hopital.getSalle(getMetier()-1);
	}

public void display() {
        
        System.out.println("Entrer le numero du choix que vous voulez \n");
        
        System.out.println("1. Envoyer les visites a la base");
        System.out.println("2. Rendre la salle disponible");
        System.out.println("3. Afficher la file d'attente \n");
        
        System.out.println("9. Se deconnecter");

    }
    
    public void appliqueChoix(int choix) throws ClassNotFoundException, SQLException {
        
        switch(choix) {
            
            case 1:
            	envoyerVisitesDansBase();
            	System.out.println("Liste de visites envoyee dans la base");
                break;
                
            case 2:
                rendreSalleDispo();
        		System.out.println("La salle est dispo");
                break;
                
            case 3:
            	System.out.println("\n\n\n" + afficherFileAttente());
                break;
              
            default :
                System.out.println("Erreur, choisissez parmis la liste");
        }
        
    }
	// envoie la liste dans base
	public boolean envoyerVisitesDansBase() throws ClassNotFoundException, SQLException {
		
		DAOVisite daoVisite = new DAOVisite();
		
		for(int index = 0; index<maxVisites;index++)
			if(this.listeVisites[index] != null){
				daoVisite.insert(this.listeVisites[index]);
				this.listeVisites[index]=null;
		}
		this.compteurVisites = 0;
		return true;
	}
	
	// ajoute une visite � la liste dans l'application
	public boolean sauvegarderVisiteDansListe() throws ClassNotFoundException, SQLException {
		
		Patient patient  = salle.getPatient();
		
		
		if(patient==null)
			return false;
		Visite visite = new Visite(this.getNom()+ " " + this.getPrenom(), this.getMetier(), patient.getId());
		
		this.listeVisites[compteurVisites]=visite;
		compteurVisites++;
		
		
		if(compteurVisites==maxVisites){
			envoyerVisitesDansBase();
		}
		
		return true;
	}

	// m�thode qui va appeller l'observer
	public boolean rendreSalleDispo() throws ClassNotFoundException, SQLException {
		
		sauvegarderVisiteDansListe();
		
		Hopital.salleInformeDisponible(this.salle);
		
		return true;
	}
}
