package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

public class Hopital {
	private static final Hopital instance = new Hopital();

	private static LinkedList<Patient> fileAttente = new LinkedList<Patient>();
	private static Salle[] salles = new Salle[2];
	private static ArrayList<Salle> sallesAbonnes = new ArrayList<Salle>();
	private static Secretaire secretaire;

	private Hopital() {

	}

	public static final Hopital getInstance() {
		return instance;
	}

	public static void abonneSalle(Salle salle) {
		sallesAbonnes.add(salle);
	}

	public static void retireSalle(Salle salle) {
		sallesAbonnes.remove(salle);
	}

	public static Salle[] getSalles() {
		return salles;
	}

	public static void setSalles(Salle[] salles) {
		Hopital.salles = salles;
	}

	public static Salle getSalle(int numeroSalle) {
		return salles[numeroSalle];
	}

	public static Secretaire getSecretaire() {
		return secretaire;
	}

	public static void setSecretaire(Secretaire secretaire) {
		Hopital.secretaire = secretaire;
	}

	public static void salleInformeDisponible(Salle bonneSalle) {
		
		for (Salle salle : sallesAbonnes) {
			if (salle == bonneSalle) 
				salle.DemandeNouveauPatient();	
		
		}
	}

	// Fonctions li�s � la file d'attente

	/*
	 * Fonction permettant de r�cup�rer l'enti�ret� de la file d'attente
	 * 
	 * Sortie : L'enti�ret� de la file d'attente | Type :
	 * LinkedList<Patient>(Liste chain�e de Patient)
	 */
	public static LinkedList<Patient> getFileAttente() {
		return fileAttente;
	}

	/*
	 * Fonction permettant d'ajouter un patient au fond de la liste d'attente
	 * 
	 * Entr�e : Un patient | Type : Patient
	 */
	public static void ajouterPatientALaFile(Patient patient) throws IOException {
		fileAttente.add(patient);
		
		exportFile(patient);
	}
	
	public static void exportFile(Patient p) throws IOException{
        File fichier = new File("file.txt");
        FileWriter writer = new FileWriter(fichier, true);
        SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String date = s.format(new Date());
        writer.write("\n" +p.getNom() +" " + p.getPrenom() + " arriv� � : " + date);
        writer.flush();
        writer.close();
    }

	/*
	 * Fonction permettant de v�rifier le prochain patient, sans le retirer de
	 * la file d'attente
	 * 
	 * Sortie : Le patient en t�te de file | Type : Patient
	 */
	public static Patient verifierProchainPatient() {
		return fileAttente.peek();
	}

	/*
	 * Fonction permettant d'appeller le prochain patient, le retirant de la
	 * file d'attente
	 * 
	 * Sortie : Le patient en t�te de file | Type : Patient
	 */
	public static Patient appellerProchainPatient() {
		return fileAttente.poll();
	}

}
