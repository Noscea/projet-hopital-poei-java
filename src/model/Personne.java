package model;

public abstract class Personne {

	protected int id;
	protected String nom;
	protected String prenom;

	public Personne() {
		super();
	}

	public Personne( int id, String nom, String prenom) {
		super();
		this.id = id ;
		this.nom = nom;
		this.prenom = prenom;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + "[id=" + id + ", nom=" + nom + ", prenom=" + prenom + "]";
	}
}
