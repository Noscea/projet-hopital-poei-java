package model;

import java.sql.SQLException;
import java.util.LinkedList;

public abstract class Personnel extends Personne {

	private int metier; // 0 = secretaire; 1 = medecin salle 1; 2 = medecin
						// salle 2

	public Personnel() {
		super();
	}

	public Personnel(int id, String nom, String prenom, int metier) {
		super(id, nom, prenom);
		this.metier = metier;
	}
	
	public int getMetier(){
		return metier;
	}

	public void authentification() {

	}
	
	public LinkedList<Patient> afficherFileAttente() {
		return Hopital.getFileAttente();
	}

	public abstract void display();
	public abstract void appliqueChoix(int choix) throws ClassNotFoundException, SQLException;

	@Override
	public String toString() {

		return super.toString() + "[metier=" + metier + "]";
	}
	
	

}
