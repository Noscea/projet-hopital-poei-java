package model;

public class Authentification {
	private String login;
	private String password;
	private String nom;
	private String prenom;
	private int metier;
	
	public Authentification(String login, String password, String nom, String prenom, int metier) {
		this.login = login;
		this.password = password;
		this.nom = nom;
		this.prenom = prenom;
		this.metier = metier;
	}
	
	public String getLogin() {
		return login;
	}
	public String getPassword() {
		return password;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public int getMetier() {
		return metier;
	}

	@Override
	public String toString() {
		return "Authentification [login=" + login + ", password=" + password + ", nom=" + nom + ", prenom=" + prenom
				+ ", metier=" + metier + "]";
	}
	
	

	
}
