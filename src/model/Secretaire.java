package model;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import dao.DAOPatient;
import dao.DAOVisite;

public class Secretaire extends Personnel {

	public Secretaire(int id, String nom, String prenom, int metier) {
		super(id, nom, prenom, metier);
	}

	public void display() {

		System.out.println("Entrer le numero du choix que vous voulez \n");

		System.out.println("1. Afficher le prochain patient");
		System.out.println("2. Afficher la file d'attente");
		System.out.println("3. Ajouter un patient a la file d'attente");
		System.out.println("4. Afficher la liste des visites du patient \n");

		System.out.println("9. Se deconnecter");

	}

	public void appliqueChoix(int choix) throws ClassNotFoundException, SQLException {

		switch (choix) {

		case 1:
			System.out.println("\n\n" + afficherProchainPatient() +"\n");
			break;
		case 2:
			System.out.println("\n\n" +afficherFileAttente()+"\n");
			break;
		case 3:
			int idPatientFile = idPatient();
			try {
				ajouterPatientAFileAttente(idPatientFile);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;

		case 4:
			int idPatientHistorique = idPatient();
			System.out.println("\n\n\n" + afficherListeVisitesPatient(idPatientHistorique));
			break;

		default:
			System.out.println("\n\n\n" + "Erreur, choisissez parmis la liste");
		}

	}

	private int idPatient() {
		@SuppressWarnings("resource")
		Scanner clavierInt = new Scanner(System.in);
		System.out.println("Quel est l'id du patient (0 si nouveau patient) ?");
		return clavierInt.nextInt();
	}

	public Patient afficherProchainPatient() {
		return Hopital.verifierProchainPatient();
	}

	public ArrayList<Visite> afficherListeVisitesPatient (int idPatient) throws ClassNotFoundException, SQLException {
		return new DAOVisite().selectByIdPatient(idPatient);
	}

	public boolean ajouterPatientAFileAttente(int idPatient) throws ClassNotFoundException, SQLException, IOException {
		DAOPatient daoPatient = new DAOPatient();
		
		//un id � 0 informe qu'il s'agit d'un nouveau patient
		if (idPatient != 0) {
			
				Patient patient = daoPatient.selectById(idPatient);
				
				//Un patient null est un patient qui n'est pas en base de donn�es. Donc un nouveau patient 
				if (patient == null){
					System.out.println("Patient inexistant. Creation d'un nouveau patient.");
					patient = creationPatient();
				}


				System.out.println("\n\n\n" +  "Ajoute a la liste : " + patient);
				
				
				Hopital.ajouterPatientALaFile(patient);
				return true;
				
		} else {
			
			Patient patient = creationPatient();
			Hopital.ajouterPatientALaFile(patient);
			return true;
		}
}
	
	@SuppressWarnings("resource")
	private Patient creationPatient() throws ClassNotFoundException, SQLException{
		
		DAOPatient daoPatient = new DAOPatient();
		
		Scanner clavierString = new Scanner(System.in);
		Scanner clavierInt= new Scanner(System.in);
		
		System.out.println("Donnez un id au patient");
		int id = clavierInt.nextInt();
		
		System.out.println("Quel est le nom du patient ?");
		String nom = clavierString.nextLine();
		
		System.out.println("Quel est le prenom du patient ?");
		String prenom = clavierString.nextLine();
		
		System.out.println("Quel est l'age du patient ?");
		int age = clavierInt.nextInt();
		
		System.out.println("Quel est le numero de telephone du patient ?");
		String telephone =  clavierString.nextLine();
		
		System.out.println("Quel est l'adresse du patient ?");
		String adresse = clavierString.nextLine();
		
		Patient patient =  new Patient(id, nom, prenom, age, telephone, adresse);
		daoPatient.insert(patient);
		
		return patient;
		
	}
}