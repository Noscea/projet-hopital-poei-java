package user;

import java.sql.SQLException;
import java.util.InputMismatchException;
import java.util.Scanner;

import dao.DAOAuthentification;
import model.Authentification;
import model.Hopital;
import model.Medecin;
import model.Personnel;
import model.Salle;
import model.Secretaire;

public class Main {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		boolean close = false;

		Authentification personneConnecte = null;

		boolean estIdentifie = false;

		Salle salle1 = new Salle(1);
		Salle salle2 = new Salle(2);

		Salle[] salles = { salle1, salle2 };

		Hopital.setSalles(salles);

		Hopital.abonneSalle(salle1);
		Hopital.abonneSalle(salle2);

		// creation hopital

		// creation salles

		// boucle d'affichage
		while (!close) {

			// pas de compte enregistr�, demande login/password
			if (!estIdentifie) {

				personneConnecte = demandeAuthentification();

				if (personneConnecte.getMetier() != 0 && personneConnecte.getMetier() != 1
						&& personneConnecte.getMetier() != 2) {
					System.out.println("les identifiants ne correspondent a aucun compte \n\n");
				} else {

					estIdentifie = !estIdentifie;

					System.out.println("Bienvenue " + personneConnecte.getNom() + " " + personneConnecte.getPrenom());
				}

			}

			// compte enregistr�, affiche les choix
			else {

				boolean afficheChoix = true;

				Medecin medecin;
				Secretaire secretaire;
				Personnel personnel;

				if (personneConnecte.getMetier() == 0) {
					secretaire = (Secretaire) initialisePersonnel(personneConnecte);
					personnel = secretaire;
				} else {
					medecin = (Medecin) initialisePersonnel(personneConnecte);
					personnel = medecin;
				}

				while (afficheChoix) {

					personnel.display();

					try {

						Scanner scannerInt = new Scanner(System.in);
						int choix = scannerInt.nextInt();

						if (choix == 9) {
							if (personnel instanceof Medecin) {

								((Medecin) personnel).sauvegarderVisiteDansListe();
								((Medecin) personnel).envoyerVisitesDansBase();
							}
							afficheChoix = !afficheChoix;
							estIdentifie = !estIdentifie;
						} else {
							personnel.appliqueChoix(choix);
						}
					} catch (InputMismatchException exception) {
						System.out.println("Erreur de saisie : vous devez saisir un chiffre");
					}

				}
			}
		}

		System.out.println("Exit");

	}

	/*
	 * Demande les login/password et tente de r�cup�rer un compte associ�
	 * retourne une instance d'authentification si trouv�
	 */

	@SuppressWarnings("resource")
	static Authentification demandeAuthentification() throws ClassNotFoundException, SQLException {

		Scanner scannerString = new Scanner(System.in);

		System.out.println("");

		System.out.println("Entrer votre login");
		String login = scannerString.nextLine();

		System.out.println("Entrer votre password");
		String password = scannerString.nextLine();

		return DAOAuthentification.selectByLoginAndPassword(login, password);
	}

	/*
	 * Initialise un secretaire ou un medecin en fonction du contenu
	 * d'authentification
	 */
	static Personnel initialisePersonnel(Authentification personneConnecte) {

		if (personneConnecte.getMetier() == 0)
			return new Secretaire(1, personneConnecte.getNom(), personneConnecte.getPrenom(),
					personneConnecte.getMetier());

		else
			return new Medecin(1, personneConnecte.getNom(), personneConnecte.getPrenom(),
					personneConnecte.getMetier());
	}

}
